<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class UsersController extends Controller
{
    public function __construct()
    {

    }

    public function store(Request $request){

        if(!Auth::guard('api')->check()){
            return  response()->json(["message"=>"You are not logged in"],403);
        }
        elseif (!$request->user()->hasRole('admin')){
            return  response()->json(["message"=>"You has not permission"],403);
        }

        $validator = Validator::make($request->all(),[
            'username' => 'required|unique:users,username',
            'email' => 'required|unique:users,email|email',
            'password' => 'required',
            'role' => 'required'

        ]);
        if($validator->fails()){
            $errors=[];
            foreach ($validator->errors()->toArray() as $k=>$v){
                $errors[]=[
                    'field'=>$k,
                    'message'=>implode("،",$v)
                ];
            }
            return  response()->json(["message"=>"Validation Failed","errors"=>$errors],422);
        }

        $user = new User();
        $user->username=$request->username;
        $user->email=$request->email;
        $user->password=bcrypt($request->password);
        $user->name=$request->name;
        $user->latitude=$request->latitude;
        $user->longitude=$request->longitude;
        $user->save();
        if($user->id){
            if(strtolower($request->role)=='customer'){
                $user->assignRole('customer');
            }
            elseif(strtolower($request->role)=='seller'){
                $user->assignRole('seller');
            }

            return  response()->json(["message"=>"User added"],401);
        }
        return  response()->json(["message"=>"Server error"],500);
    }



}
