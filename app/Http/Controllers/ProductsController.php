<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProductsController extends Controller
{
    public function __construct()
    {

    }

    public function store(Request $request){
        if(!Auth::guard('api')->check()){
            return  response()->json(["message"=>"You are not logged in"],403);
        }
        elseif (!$request->user()->hasRole('seller')){
            return  response()->json(["message"=>"You has not permission"],403);
        }
        
        $validator = Validator::make($request->all(),[
            'title' => 'required',
            'code' => 'required|unique:products,code',
            'price' => 'required'

        ]);
        if($validator->fails()){
            $errors=[];
            foreach ($validator->errors()->toArray() as $k=>$v){
                $errors[]=[
                    'field'=>$k,
                    'message'=>implode("،",$v)
                ];
            }
            return  response()->json(["message"=>"Validation Failed","errors"=>$errors],422);
        }

        $product = new Product();
        $product->user_id=Auth::guard('api')->id();
        $product->code=$request->code;
        $product->title=$request->title;
        $product->price=$request->price;
        $product->save();
        if($product->id){
            return  response()->json(["message"=>"Product added"],401);
        }
        return  response()->json(["message"=>"Server error"],500);
    }

    public function list(Request $request){
        if(!Auth::guard('api')->check()){
            return  response()->json(["message"=>"You are not logged in"],403);
        }
        elseif (!$request->user()->hasRole('customer')){
            return  response()->json(["message"=>"You has not permission"],403);
        }
        $longitude=Auth::guard('api')->user()->longitude;
        $latitude=Auth::guard('api')->user()->latitude;

        $sellers= DB::select("SELECT * FROM
                    (
                    SELECT u.id, (6371 * acos(cos(radians(". $latitude .")) * cos(radians(latitude)) *
                    cos(radians(longitude) - radians(". $longitude .")) +
                    sin(radians(". $latitude .")) * sin(radians(latitude))))
                    AS distance
                    FROM users AS u
                    LEFT JOIN model_has_roles AS mhr ON mhr.model_id = u.id AND mhr.model_type='App\\\User'
                    LEFT JOIN roles AS r ON r.id = mhr.role_id
                    WHERE r.name='seller' 
                    ) AS distances
                WHERE distance < 5
                ");

        $sellers = array_column($sellers, 'id');
        $products=Product::whereIn('user_id',$sellers)->get();

        return response()->json($products);

    }

}
