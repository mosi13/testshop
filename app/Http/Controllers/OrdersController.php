<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrdersController extends Controller
{
    public function __construct()
    {
    }

    public function store(Request $request){
        if(!Auth::guard('api')->check()){
            return  response()->json(["message"=>"You are not logged in"],403);
        }
        elseif (!$request->user()->hasRole('customer')){
            return  response()->json(["message"=>"You has not permission"],403);
        }

        $products=Product::whereIn('id',$request->product_id)->get();
        $item = [];
        $totalPrice=0;
        foreach($products as $v){
            $item[$v->id]=[
                'price'=>$v->price
            ];
            $totalPrice += $v->price;
        }

        $order= new Order();
        $order->user_id = Auth::guard('api')->id();
        $order->price = $totalPrice;
        $order->save();

        $order->order_items()->attach($item);

        if($order->id){
            return  response()->json(["message"=>"Order added"],401);
        }
        return  response()->json(["message"=>"Server error"],500);
    }



}
