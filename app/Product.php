<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function order_items(){
        return $this->belongsToMany(OrderItem::class,'order_items')->withPivot('price');
    }
}
