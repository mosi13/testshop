<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function order_items(){
        return $this->belongsToMany(Product::class,'order_items')->withPivot('price');
    }
}
