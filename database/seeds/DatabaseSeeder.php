<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(RoleTableSeeder::class);
    }

}

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users')->truncate();
        DB::table('users')->insert([
            ['username' => 'administrator','password'=>bcrypt('123456'),'role'=>'ADMIN','email'=>'admin@admin.com','name'=>'admin',]
        ]);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }

}

class RoleTableSeeder extends Seeder {

    public function run()
    {
        $role = Role::create(['name' => 'admin']);
        $permission = Permission::create(['name' => 'add user']);
        $role->givePermissionTo($permission);

        $role = Role::create(['name' => 'seller']);
        $permission = Permission::create(['name' => 'add product']);
        $role->givePermissionTo($permission);

        $role = Role::create(['name' => 'customer']);
        $permission = Permission::create(['name' => 'buy product']);
        $role->givePermissionTo($permission);

        $admin= \App\User::find(1);
        $admin->assignRole('admin');

    }

}

